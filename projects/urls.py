from django.urls import path
from django.shortcuts import redirect
from projects.views import list_projects, show_project, create_project


def redirect_to_projects(request):
    return redirect("projects")


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("projects/", redirect_to_projects, name="projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
